# OpenML dataset: World-Top-Chess-Players-(August-2020)

https://www.openml.org/d/43566

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The International Chess Federation (FIDE) governs international chess competition. FIDE used Elo rating system for calculating the relative skill levels of players.
Content
The dataset contains details of all the chess players in the world sorted by their Standard FIDE rating (highest to lowest) as updated by FIDE in August 2020. The data includes all active and inactive players which can be identified by the Inactive_flag column.
Note: All ratings are updated as published by FIDE in August 2020.
Acknowledgements
FIDE: https://www.fide.com/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43566) of an [OpenML dataset](https://www.openml.org/d/43566). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43566/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43566/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43566/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

